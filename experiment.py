# -*- coding: utf-8 -*-

import Xtkinter as tk
import numpy as np
import random as rndm
from math import *
import time
import os
from shapes import *
from experiment_buttons import buttons as bt
import numpy as np
import threading
import multiprocessing 

import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import pylab as plab

colors = {"circle" : "blue", "square" : "red", "triangle" : "yellow", "none" : "black", "unknown" : "purple"}
languages = ["Français","Italiano","Deutch","English"]

class Experiment(object):

    def __init__(self, parent, shape = None):

        self.plot_h = 500
        self.plot_w = 600 #int(0.7 * self.plot_h)
        parent.title('EXPERIMENT')
        self.parent = parent
        
        self.shape = None
        self.curshape = None
        
        self.ball_size = 0.008
        self.scale = self.plot_h # Use hight of table in m
        self.x_offset = ( float(self.plot_w) / self.plot_h ) / 2.
        self.y_offset = 0.
        self.launch_pad = 0.2
        self.side = 0.08
        
        self.angle = 30
        self.acc = -9.8 * sin(self.angle*pi/180.)
        self.dt = 0.01
        self.isblind = False
        self.speed = 1
        
        self.lang = "fr"
        
        self.pts = { "square" : [], "circle" : [], "triangle" : [], "none" : [], "unknown" : []}
        self.hist = {}
        
        self.prevnpts = 0
        self.npts = 0
        self.threads = []
        self.demorunning = False
        
        self.initialize()
        
    def change_lang(self,*args) :
        
        lang = self.lang_variable.get()
        if "It" in lang : self.lang = "it"
        elif "Fr" in lang : self.lang = "fr"
        elif "En" in lang : self.lang = "en"
        elif "Deutch" in lang : self.lang = "de"
        self.lang_variable.set(lang)
        
        for widget in self.parent.winfo_children():
            widget.destroy()

        if "Fr" in lang : 
            languages.remove('Fran\xc3\xa7ais')
            languages.insert(0,'Fran\xc3\xa7ais')
        else : 
            languages.remove(lang)
            languages.insert(0,lang)
        
        self.initialize()
        
    def initialize(self):

        self.right_frame = tk.Frame(self.parent)
        self.right_frame.pack(side=tk.LEFT)
        
        # Top frame for buttons
        self.top_frame = tk.Frame(self.right_frame)
        self.top_frame.pack(side=tk.TOP)
        
        #stop_button = tk.Button(self.top_frame, text=bt["stop"][self.lang], width=20, command=self.stop)
        #stop_button.pack(side = tk.RIGHT)   
        reset_button = tk.Button(self.top_frame, text=bt["reset"][self.lang], width=30, command=self.reset)
        reset_button.pack(side = tk.RIGHT)   
        blind_button = tk.Button(self.top_frame, text=bt["blind"][self.lang], width=30, command=self.blind)
        blind_button.pack(side = tk.RIGHT)   

        self.canvas_frame = tk.Canvas(self.right_frame)
        self.canvas_frame.pack(side = tk.TOP)
        
        self.help_text = tk.Label(self.right_frame, text=bt["help"][self.lang] )
        self.help_text.pack(side = tk.BOTTOM, fill=tk.X)
        
        self.canvas = tk.Canvas(self.canvas_frame,width=self.plot_w,height=self.plot_h)
        self.canvas.bind("<Button-1>", self.callback)
        self.canvas.pack(side = tk.TOP)

        # Settings frame on the left
        self.settings_frame = tk.Frame(self.parent,bg="grey")
        self.settings_frame.pack(side=tk.RIGHT, fill=tk.Y)
        self.settings = tk.Frame(self.settings_frame,bg="grey")
        self.settings.pack(anchor=tk.CENTER)
          
        obstacle_text = tk.Label(self.settings, width=50, text=bt["obst"][self.lang],bg="grey" )
        obstacle_text.pack(side = tk.TOP)      
        
        circle_button = tk.Button(self.settings, text=bt["circle"][self.lang], width=15, command=self.use_circle, bg="grey")
        circle_button.pack(side = tk.TOP)
        circle_button.config(highlightbackground="grey")
        square_button = tk.Button(self.settings, text=bt["square"][self.lang], width=15, command=self.use_square, bg="grey")
        square_button.pack(side = tk.TOP)
        square_button.config(highlightbackground="grey")
        triangle_button = tk.Button(self.settings, text=bt["triangle"][self.lang], width=15, command=self.use_triangle, bg="grey")
        triangle_button.pack(side = tk.TOP)
        triangle_button.config(highlightbackground="grey")
        rndshape_button = tk.Button(self.settings, text=bt["rndshape"][self.lang], width=15, command=self.use_random_shape, bg="grey")
        rndshape_button.pack(side = tk.TOP)
        rndshape_button.config(highlightbackground="grey")
  
        line1 = tk.Frame(self.settings,height=1,width=200,bg="black")
        line1.pack(side = tk.TOP, pady=20)        
        
        self.nballs_bar = tk.Scale(self.settings, from_=2, to=20, orient=tk.HORIZONTAL, 
            label=bt["nballs"][self.lang], length=200, bg="grey")
        self.nballs_bar.pack(side = tk.TOP)
        newball_button = tk.Button(self.settings, text=bt["throw"][self.lang], width=30, command=self.new_ball, bg="grey")
        newball_button.pack(side = tk.TOP, pady=5)
        newball_button.config(highlightbackground="grey")
        
        line4 = tk.Frame(self.settings,height=1,width=200,bg="black")
        line4.pack(side = tk.TOP, pady=20)        
        
        demo_button = tk.Button(self.settings, text=bt["demo"][self.lang], width=15, command=self.call_demo, bg="grey")
        demo_button.pack(side = tk.TOP)
        demo_button.config(highlightbackground="grey")
                      
        line3 = tk.Frame(self.settings,height=1,width=200,bg="black")
        line3.pack(side = tk.TOP, pady=20)        
        
        self.speed_bar = tk.Scale(self.settings, from_=0., to=2., resolution=0.01, 
            orient=tk.HORIZONTAL, label=bt["speed"][self.lang], length=200,bg="grey")
        self.speed_bar.set(self.speed)
        self.speed_bar.pack(side = tk.TOP)
        self.angle_bar = tk.Scale(self.settings, from_=1., to=80., resolution=0.01, 
            orient=tk.HORIZONTAL, label=bt["angle"][self.lang], length=200,bg="grey")
        self.angle_bar.set(self.angle)
        self.angle_bar.pack(side = tk.TOP)
        self.bounce_bar = tk.Scale(self.settings, from_=0., to=1., resolution=0.01, 
            orient=tk.HORIZONTAL, label=bt["bounce"][self.lang], length=200,bg="grey")
        self.bounce_bar.set(0.4)
        self.bounce_bar.pack(side = tk.TOP)
           
        line2 = tk.Frame(self.settings,height=1,width=200,bg="black", pady=50)
        line2.pack(side = tk.TOP, pady=20)        
        
        text = tk.Label(self.settings, width=50, text=bt["lang"][self.lang],bg="grey" )
        text.pack(side = tk.TOP)
        self.lang_variable = tk.StringVar(self.settings)
        self.lang_variable.set(languages[0])
        self.lang_variable.trace("w",self.change_lang)
        self.lang_menu = tk.OptionMenu(self.settings, self.lang_variable, *languages)
        self.lang_menu.pack(side = tk.TOP)
        self.lang_menu.config(highlightbackground="grey")
        self.lang_menu.config(bg="grey")
        
        self.make_hist()
    
    def blind(self) :
        
        if self.isblind :
            self.canvas.delete(self.blindfold)
            self.isblind = False
        else :
            self.blindfold = self.canvas.create_rectangle(0, self.plot_h/4.,
            self.plot_w, self.plot_h * 3./4., fill = "red")
            self.isblind = True
    
    def reset(self) :
        
        self.canvas.delete("all")
        self.shape = None
        self.curshape = None
        self.isblind = False
        self.pts = { "square" : [], "circle" : [], "triangle" : [], "none" : [], "unknown" : []}
        self.make_hist()
        
    def use_random_shape(self) :
    
        shape = rndm.choice([self.use_circle,self.use_square,self.use_triangle])
        shape(True)
    
    def draw_shape(self,name, l) :
    
        if name == "circle" :
            return self.canvas.create_oval((l+self.x_offset)*self.scale, (1.-0.5-l+self.y_offset)*self.scale,
            (-l+self.x_offset)*self.scale, (1.-0.5+l+self.y_offset)*self.scale, fill = "grey")
        elif name == "square" :
            return self.canvas.create_rectangle( (l+self.x_offset)*self.scale, (1.-0.5-l+self.y_offset)*self.scale,
            (-l+self.x_offset)*self.scale, (1.-0.5+l+self.y_offset)*self.scale, fill = "grey")
        elif name == "triangle" :
            return self.canvas.create_polygon(
            ( (self.x_offset - l )*self.scale, (self.y_offset + 0.5 + sqrt(3)/2.*l)*self.scale,
            (self.x_offset + l )*self.scale  , (self.y_offset + 0.5 + sqrt(3)/2.*l)*self.scale,
            (self.x_offset)*self.scale,        (self.y_offset + 0.5 - sqrt(3)/2.*l)*self.scale )
            , fill = "grey") 
    
    def use_shape(self,name,size,leave = False) :
    
        if name not in ["circle","square","triangle"] : return
        
        self.canvas.delete("all")
        if self.isblind : 
           self.isblind = False
           self.blind()
        
        if name == "circle" : shape = Circle
        elif name == "square" : shape = Square
        elif name == "triangle" : shape = Triangle
        
        if self.curshape is None :
            self.shape = shape(size,[0.,0.5])
            self.curshape = self.draw_shape(name,size)
        elif self.shape.name == name and not leave:
            self.canvas.delete(self.curshape)
            self.curshape = None
            self.shape = None
        else :
            self.shape = shape(size,[0.,0.5])
            self.canvas.delete(self.curshape)
            self.curshape = self.draw_shape(name,size)
            
        if self.isblind :
            self.canvas.tag_raise(self.blindfold)
    
    def use_circle(self, leave = False) :
    
        self.use_shape("circle",self.side,leave)
                        
    def use_square(self, leave = False) :
        
        self.use_shape("square",self.side,leave)
        
    def use_triangle(self, leave = False) :
        
        self.use_shape("triangle",self.side,leave)
       
    def scaled(self, points, reverse = False) :
        res = []
        if not reverse :
            for p in points :
                res.append( ( (p[0]+self.x_offset)*self.scale, (1-p[1]+self.y_offset)*self.scale) )
        else :
            for p in points :
                res.append( ( p[0]/float(self.scale)-self.x_offset, -(-1+p[1]/float(self.scale)-self.y_offset) ) )
        return res
     
    def new_ball(self, nballs=0, delay = 0 ) :
    
        if nballs == 0 : nballs = int(self.nballs_bar.get())
        if delay == 0 and nballs > 1 : delay = 0.5
        for i in range(nballs) :
            x = rndm.uniform(-self.launch_pad/2.,self.launch_pad/2.)
            self.propagate((x,1), delay*i)

    def call_demo(self) :
        
        tdemo = threading.Thread(target=self.demo)
        self.demorunning = True
        tdemo.start()

    def demo(self) :
        
        self.isblind = False
        self.canvas.delete(self.curshape)
        self.curshape = None
        self.shape = None
        self.speed = 2
        self.angle_bar.set(30)
        self.bounce_bar.set(0.2)
        
        self.use_circle()
        for i in range(4) :
            self.new_ball(5)
            if i != 3 : time.sleep(5)
        for t in self.threads : t.join()
        time.sleep(0.2)
        self.use_square()
        for i in range(4) :
            self.new_ball(5)
            if i != 3 : time.sleep(5)
        for t in self.threads : t.join()
        time.sleep(0.2)
        self.use_triangle()
        for i in range(4) :
            self.new_ball(5)
            if i != 3 : time.sleep(5)
        for t in self.threads : t.join()
        self.speed = 1
        self.demorunning = False
 
    def propagate(self,start,delay = 0) :
     
        t = threading.Timer(delay, self.propagate_job,args=(start,))
        self.threads.append(t)
        t.start()
        
    def propagate_job(self,start) :
    
        x0 = start[0]
        y0 = start[1]
        
        x, y = x0, y0
        vx, vy = 0., 0.
        ppt = -1

        self.speed = ( - self.speed_bar.get() + 2 )
        self.angle = self.angle_bar.get()
        self.acc = -9.8 * sin(self.angle*pi/180.)
        if self.shape is not None :
            self.shape.bounce_factor = self.bounce_bar.get()
        
        timepassed = 0
        y_before = 100
        x_before = 100
        skip = False
        
        while y > 0. :
            
            dy = vy * self.dt + 0.5 * self.acc * self.dt*self.dt
            dx = vx * self.dt

            if self.shape is not None and self.shape.is_in([x+dx,y+dy]) :
                vx, vy = self.shape.bounce((x,y),(x+dx,y+dy),(vx,vy))
                x += vx*self.dt
                y += vy*self.dt
                
                if 0. < abs(y+dy - y_before) < 0.004 and 0. < abs(x+dy - x_before) < 0.001 : 
                    skip = True
                    y += vy*self.dt
                    self.canvas.delete(ppt)
                    if not self.isblind : self.draw_point((x,y))
                    break
                
            else :
                vy += self.acc * self.dt
                y+=dy
                x+=dx

            timepassed += self.dt*self.speed
            
            if ppt != -1 : 
                self.canvas.delete(ppt) 
                ppt = -1
                
            if timepassed > 0.005 and not( self.isblind and 0.25 < y+dy < 0.75) :
                ppt = self.draw_point((x,y))
                timepassed = 0
            
            self.right_frame.update_idletasks()
            
            x_before = x
            y_before = y
            
            if self.speed > 0. :
                time.sleep(self.dt*self.speed)
        
        vx, vy = 0., 0.
        self.canvas.delete(ppt)
        if skip : return
        self.draw_point((x,0))
        if self.isblind :               self.pts["unknown"].append(x)
        elif self.shape is not None :   self.pts[self.shape.name].append(x)
        else :                          self.pts["none"].append(x)
        self.npts +=1
        self.right_frame.update_idletasks()
             
    def callback(self,event) :
        
        point = ( event.x, event.y )
        scaled_point = self.scaled([point], reverse=True)[0]
        self.propagate(scaled_point)

    def make_hist(self) :
        
        if self.npts == 0 : 
            self.parent.after(500, self.make_hist)
            return
        if self.npts == self.prevnpts : 
            self.parent.after(500, self.make_hist)
            return
        self.prevnpts = self.npts
        
        plt.cla()

        plt.ylabel(bt["plotx"][self.lang])
        plt.xlabel(bt["ploty"][self.lang])
        plt.axis([-1, 1, 0, 30])
        
        for name,pts in self.pts.items() :
            if len(pts) > 0 :
                if name == "unknown" : 
                    self.hist[name] = plt.hist(pts, bins=[ x/10. for x in range(-10, 10)], facecolor=colors[name], label=bt["unknown"][self.lang], zorder=10, alpha=0.75)
                else :
                   labelname = bt["nothing"][self.lang]
                   if name in ["circle","square","triangle"] : labelname = bt[name+"_"][self.lang]
                   self.hist[name] = plab.hist(pts, bins=[ x/10. for x in range(-10, 10)], facecolor=colors[name], label=labelname)
        
        legend = plt.legend(loc='upper center', shadow=True)
        
        plt.grid(True)
        plt.show(block=False)
        fig = plt.gcf()
        fig.set_size_inches(5, 3, forward=True)
        fig.canvas.manager.window.wm_geometry("+800+500")
        fig.canvas.manager.window.attributes('-topmost', 1)
        fig.canvas.manager.window.attributes('-topmost', 0)
        self.parent.after(500, self.make_hist)

    def draw_point(self,p, fill = "blue") :
        
        r = self.ball_size
        x1 = int((p[0]+r+self.x_offset)*self.scale)
        y1 = int((1.0-p[1]-r+self.y_offset)*self.scale) 
        x2 = int((p[0]-r+self.x_offset)*self.scale)
        y2 = int((1.-p[1]+r+self.y_offset)*self.scale)
        return self.canvas.create_oval( x1, y1, x2, y2 , fill = fill, width = 0)

            
            
                        
                        
        
if __name__ == '__main__':
    
    root = tk.Tk()  # Instantiate a root window
    b = Experiment(root)  # Instantiate a Game object
    root.mainloop()  # Enter the main event loop
