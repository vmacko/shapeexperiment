import math
import random as rndm

class Shape(object) :

    name = None
    pos = [0,0]
    picture = None
    bounce_factor = 0.4
    bounce_angle = 0.1
    
    def __init__(self, pos = [0,0]) :
        self.name = None
        self.pos = pos
        self.picture = None
        self.bounce_factor = 0.4
        self.bounce_angle = 0.05
        
class Circle(Shape) :
    
    def __init__(self, r, pos = [0.,0.]) :
        super(Shape,self).__init__()
        
        self.name = "circle"
        self.r = r
        self.pos = pos

    def is_in(self,pt) :
    
        if( math.sqrt( (pt[0] - self.pos[0])**2 + (pt[1] - self.pos[1])**2 ) <= self.r ) : return True
        return False
    
    def find_pt_on_edge(self, p1, p2) :
        return
    
    def bounce(self,p1,p2,v) :
        x = p1[0]
        y = math.sqrt( self.r**2 - (x - self.pos[0])**2 ) + self.pos[1]
        
        m = - (x - self.pos[0])/(y - self.pos[1])
        th_to_x = math.atan(m)
        ball_th = math.pi / 2. - th_to_x
        angle = - 2. * ball_th * 180. / math.pi
        
        red_v = scale(v,self.bounce_factor,ball_th * 180. / math.pi)
        
        return rotate(red_v,angle)
        
class Square(Shape) :
    
    def __init__(self, side, pos = [0.,0.]) :
        super(Shape,self).__init__()
        
        self.name = "square"
        self.side = side
        self.pos = pos

    def is_in(self,pt) :
    
        if( 
            abs(pt[0] - self.pos[0]) <= self.side and 
            abs(pt[1] - self.pos[1]) <= self.side 
            ) : return True
        return False
    
    def bounce(self,p1,p2,v) :
        if v[0] == 0 :
            return (rndm.choice([1,-1])*0.1*v[1],-self.bounce_factor*v[1])
        else :
            return (self.bounce_factor*v[0],-self.bounce_factor*v[1])
    
        
class Triangle(Shape) :
    
    def __init__(self, side, pos = [0.,0.]) :
        super(Shape,self).__init__()
        
        self.name = "triangle"
        self.side = side
        self.pos = pos

    def is_in(self,pt) :
    
        if( 
            abs(pt[1] - self.pos[1]) <= self.side * math.sqrt(3) / 2. and 
            (pt[1] - self.pos[1]) <= + math.sqrt(3) * ( - pt[0] - self.pos[0] + self.side / 2. ) and
            (pt[1] - self.pos[1]) <= + math.sqrt(3) * ( pt[0] - self.pos[0] + self.side / 2. )
        ) : return True
        
        return False
    
    def bounce(self,p1,p2,v) :
        
        v = scale(v,self.bounce_factor,60.)
        if p2[0] - self.pos[0] > 0. :
            return rotate(v,60.)
        else :
            return rotate(v,-60.)

def rotate(v, angle) : 

    th = angle / 180 * math.pi
    return (
        v[0] * math.cos(th) - v[1] * math.sin(th),
        v[0] * math.sin(th) + v[1] * math.cos(th)
         )
         
def scale(v, factor, angle) :

    c = (1 - factor) * abs(math.sin(angle))
    #print angle, abs(math.sin(angle)), c, 1 - c, v, ( v[0] *  (1 - c), v[1] * (1 - c) )	 
    return ( v[0] *  (1 - c), v[1] * (1 - c) )	 