# -*- coding: utf-8 -*-

buttons = {
        "obst"      : { "it" : "Scegli la forma dell'ostacolo", "en" : "Choose the shape of the obstacle", "fr" : "Choisissez la forme de l'obstacle", "de" : "Wählen die Form des Hindernisses" },
        "reset"     : { "it" : "Reset", "en" : "Reset", "fr" : "Reset", "de" : "Rücksetzen" },
        "stop"      : { "it" : "Stop", "en" : "Stop", "fr" : "Stop", "de" : "Stop" },
        "blind"     : { "it" : "Mostra/Nascondi", "en" : "Blind/Unblind", "fr" : "Montrer/Cacher", "de" : "Anzeigen/verstecken" },
        "circle"    : { "it" : "Cerchio", "en" : "Circle", "fr" : "Cercle", "de" : "Zirkel" },
        "square"    : { "it" : "Quadrato", "en" : "Square", "fr" : "Carré" , "de" : "Quadrat"},
        "triangle"  : { "it" : "Triangolo", "en" : "Triangle", "fr" : "Triangle", "de" : "Triangel" },
        "circle_"   : { "it" : "Cerchio", "en" : "Circle", "fr" : "Cercle", "de" : "Zirkel" },
        "square_"   : { "it" : "Quadrato", "en" : "Square", "fr" : "Carre'" , "de" : "Quadrat"},
        "triangle_" : { "it" : "Triangolo", "en" : "Triangle", "fr" : "Triangle", "de" : "Triangel" },
        "rndshape"  : { "it" : "Forma random", "en" : "Random shape", "fr" : "Forme aléatoire", "de" : "Zufällige Form"},
        "nballs"    : { "it" : "Quale palline vuoi lanciare?", "en" : "How many marbles to throw?", "fr" : "Lancer combien de billes?" , "de" : "Wie viele Bälle zu werfen?"},
        "throw"     : { "it" : "Lancia tante palline", "en" : "Throw many marbles", "fr" : "Lancer plusieurs billes", "de" : "Werfen viele Bälle" },
        "speed"     : { "it" : "Velocità simulazione", "en" : "Simulation speed", "fr" : "Vitesse de la simulation" , "de" : "Simulationsgeschwindigkeit"},
        "lang"      : { "it" : "Linguaggio", "en" : "Language", "fr" : "Language", "de" : "Sprache" },
        "plotx"     : { "it" : "N palline", "en" : "N marbles", "fr" : "N billes", "de" : "N Balle"},
        "ploty"     : { "it" : "x", "en" : "x", "fr" : "x", "de" : "x"},
        "nothing"   : { "it" : "Nessun ostacolo", "en" : "No obstacle", "fr" : "Pas d'obstacle", "de" : "Kein Hindernis"},
        "demo"      : { "it" : "Lancia demo", "en" : "Launch demo", "fr" : "Lancer dimostration", "de" : "Startet Demo"},
        "unknown"   : { "it" : "????", "en" : "????", "fr" : "????", "de" : "????"},
        "angle"     : { "it" : "Inclinazione tavolo (°)", "en" : "Table inclintion (°)", "fr" : "Inclination de la table (°)", "de" : "Neigung des Tisches (°)"},
        "bounce"    : { "it" : "Forza rimbalzo", "en" : "Bouncing strenght", "fr" : "Force du rebond", "de" : "Prallen Stärke"},
        "help"      : { 
        
            "it" : "Clicca sullo schermo per lanciare una pallina o lanciane tante con il bottone.\n\
            Nel menu sulla destra puoi scegliere un'ostacolo da mettere sul tavolo!\n\
            Vuoi metterti alla prova? Nascondi l'ostacolo con il bottone in alto e seleziona una forma random!",
            "en" : "Click on the screen to throw a marble or throw many with the button.\n\
            In the right menu you can select an obstable to place in the middle!\n\
            Do you want to test yourself? Hide the shape with the button above and select a random shape!", 
            "fr" : "Cliquer sur l'écran pour lancer une bille ou sur le button pour en lacer plusieurs.\n\
            Dans le menu sur la droite vous pouvez choisir un'obstacle à placer sur la table!\n\
            Voulez vous vous tester? Cachez l'obstacle avec le button au dessus et selection un forme aléatorie!",
             "de" : "Klicken Sie auf den Bildschirm, um einen Ball zu werfen oder viele mit der Taste werfen.\n\
             Im rechten Menü können Sie ein Hindernis wählen in der Mitte zu platzieren!\n\
             Wollen Sie sich selbst zu testen? Blenden Sie die Form mit der Taste oben und wählen Sie eine beliebige Form!"}
}
